$(function() {

	$('#ecocard-label').click(function() {
		$('body, html').animate({
			scrollTop: $(".ecocard").offset().top
		}, 800);
		return false;
	});
	$('#tomap').click(function() {
		$('body, html').animate({
			scrollTop: $("#tomap").offset().top
		}, 1000);
		return false;
	});
	$('.shop-map').click(function() {
		$('body, html').animate({
			scrollTop: $("#map").offset().top
		}, 800);
		mapMarkerClick(points[$(this).closest('.shop').attr('data-shop')].pm, true);
		return false;
	});
	$('.shop-pic, .shop-pic-more').colorbox({
		//rel: 'gallery1',
		transition: 'elastic',
		opacity: false,
		current: false,
		maxWidth: '90%',
		maxHeight: '90%'
	});
	if ($("#colorbox").swipe) {
		$("#colorbox").swipe({
			//Generic swipe handler for all directions
			swipeLeft: function(event, direction, distance, duration, fingerCount) {
				$.colorbox.prev();
			},
			swipeRight: function(event, direction, distance, duration, fingerCount) {
				$.colorbox.next();
			},
			//Default is 75px, set to 0 for demo so any distance triggers swipe
			threshold: 0
		});
	}
	$('input[placeholder]').placeholder();
	$('.shop input').click(function() {
		$('#price-window input[name="shop"]').val($(this).closest('.shop').attr('data-shop'));
		$.colorbox({
			html: $('#price-window').html(),
			opacity: false,
			maxWidth: '90%'
		});
		return false;
	});
	$(document).on('submit', '.price-inner', function() {
		$.ajax({
			type: "POST",
			url: "mail.php",
			data: $(this).find('input'),
			dataType: 'json',
			success: function(r) {
				if (r.status) $('#colorbox .price-inner').html('Запрос успешно отправлен');
				else alert('Ошибка при отправке запроса');
			}
		});
		return false;
	});
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDECvItT-eLyOVW8Beo3yYLtoeuk2I-nSo&sensor=false&callback=gmapready";
	document.body.appendChild(script);
});
var points = [{
	h: 'Хоздвор-Боровая',
	t: 'Минский р-н.<br>п. Боровая, 6а',
	y: 53.964528,
	x: 27.646306
}, {
	h: 'Хоздвор-Жуков луг',
	t: 'Минский р-н.,<br>д. Жуков Луг, 1б',
	y: 53.975111,
	x: 27.766778
}, {
	h: 'Хоздвор-Стиклево',
	t: 'д. Большое Стиклево,<br>ул. Восточная, 1',
	y: 53.873111,
	x: 27.695417
}, {
	h: 'Хоздвор-Острошицкий Городок',
	t: 'а.г. Острошицкий Городок,<br>ул. Ленинская, 46В',
	y: 54.067750,
	x: 27.697415
}],
// 54.067750, 27.697415
	map, infowindow, balloonPlacemark;

function gmapready() {
	var mapOptions = {
		center: new google.maps.LatLng(53.904449, 27.566140),
		zoom: 12,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
		streetViewControl: false
	};
	map = new google.maps.Map(document.getElementById("map"), mapOptions);
	infowindow = new google.maps.InfoWindow({
		content: ""
	});
	var bounds = new google.maps.LatLngBounds();
	bounds.extend(new google.maps.LatLng(53.902599, 27.411948));
	bounds.extend(new google.maps.LatLng(53.842278, 27.497092));
	var icon={
		url: '/img/marker.png',
		anchor: new google.maps.Point(30, 30)
	}
	for (var i = 0; i < points.length; i++) {
		points[i].pm = new google.maps.Marker({
			position: new google.maps.LatLng(points[i].y, points[i].x),
			map: map,
			icon: icon,
			html: '<div class="map-infowindow"><h2>' + points[i].h + '</h2><div>' + points[i].t + '</div></div>'
		});
		bounds.extend(points[i].pm.getPosition());
		google.maps.event.addListener(points[i].pm, 'click', function() {
			mapMarkerClick(this);
		});
	}
	map.fitBounds(bounds);
	google.maps.event.addListener(map, "click", function() {
		mapMarkerClick();
	});
	google.maps.event.addListener(infowindow, "closeclick", function() {
		balloonPlacemark = false;
	});
}

function mapMarkerClick(marker, zoom) {
	if (zoom) {
		map.panTo(marker.getPosition());
		map.setZoom(13);
	}
	if (marker && balloonPlacemark != marker) {
		//this.setIcon(that.iconPMc);
		balloonPlacemark = marker;
		//setCookie('map_click_'+this.dataType+'_'+this.dataId,1,false,'/',CookieDomain);
		infowindow.setContent(marker.html);
		infowindow.open(map, marker);
	} else {
		infowindow.close();
		balloonPlacemark = false;
	}
}